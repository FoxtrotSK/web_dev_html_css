# Find the longest substring having equal number of 1's and 0's
def longest_10_substring(s1):
    l_s1 = len(s1)
    sub_string_list = []
    res = ''
    res1 = 0
    for i in range(l_s1):
        for j in range(i+1, l_s1+1):
            sub_string_list.append(s1[i:j])

    sub_string_list.sort(key=len)
    # print(sub_string_list)
    for char in sub_string_list[::-1]:
        if char.count('0') == char.count('1'):
            res = char
            res1 = len(char)
            break
    return res, res1


if __name__ == '__main__':
    my_str = input()
    result = longest_10_substring(my_str)
    print(result)