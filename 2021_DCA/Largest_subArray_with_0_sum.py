# Brute Force Approach
def zero_sum(arr):
    l_arr = len(arr)
    sub_arr = []
    # Create all possible sub_arrays
    for i in range(l_arr):
        for j in range(i+1, l_arr+1):
            sub_arr.append(arr[i:j])

    zero_sum_arr = []
    for sub_a in sub_arr:
        if sum(sub_a) == 0:
            zero_sum_arr.append(sub_a)
    zero_sum_arr.sort(key=len)
    if len(zero_sum_arr) == 0:
        return zero_sum_arr, 0
    else:
        ans = zero_sum_arr[-1]
        return ans, len(ans)


if __name__ == '__main__':
    ip_arr = [int(x) for x in input().split()]
    result = zero_sum(ip_arr)
    print(result)
