courses = ['Math', 'History', 'Physics', 'CompSci']

print(courses)

# To add an item to the list we can use append(), insert() and extend()

courses.append('Arts')
print(courses)

# To add an item at a specific location we use insert
print('I want to add Biology at 2nd position')

# ***************************************************************************************************
# The first argument to insert is the index position and the next one is the element you want to insert

courses.insert(1, 'Biology')
print(courses)

# Say you want to add multiple items like another list to an existing list
courses_2 = ['AnalogElectronics', 'Geography', 'Chemistry']
courses.extend(courses_2)
print(courses)

courses_3 = ('EVS', 'DigitalElectronics')
# courses.append(courses_3)
# print(courses)
# ['Math', 'Biology', 'History', 'Physics', 'CompSci', 'Arts', 'AnalogElectronics', 'Geography', 'Chemistry', ('EVS', 'DigitalElectronics')]
# This is not what we want. We want to add the elements individually
# Hence we use extend() and not append() or insert() when we have multiple elements
courses.extend(courses_3)
print(courses)


# To remove specific element from the list we can use remove(). If element is not present we will get ValueError

courses.remove('EVS')
print(courses)

# To remove the last element we can use pop()
# pop() will actually return the last element which we can store in another variable if we want

courses.pop()
print(courses)

# To sort a list inplace use sort()
nums = [1, 453, 1254, 5, 2, 6, 43, 3]
print(nums)
nums.sort()
print(nums)
# To print in descending order
nums.sort(reverse=True)
print(nums)

# To create a copy of the sorted list and keep the original list intact use sorted()

sorted_list_asc = sorted(nums)
print(sorted_list_asc)
print(nums)

# You can use min max and sum on the list as well
print(f'The min value in nums list is {min(nums)}')
print("The max value in nums list is {}".format(max(nums)))
print(f'The sum of the elements of the nums list is {sum(nums)}')

# To find Index of any element we can use index()

print(courses.index('Physics'))

for idx, course in enumerate(courses):
	print(idx, course)

print('#######################################################################################################')
# If I want to start the index from 1 I can simply do that by adding an argument of start and equating it to 1
# By default it is zero

for idx, course in enumerate(courses, start=1):
	print(idx, course)



# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


# Tuples

# Similar to list but are immutable. Meaning once created, they cannot be changed

print(dir(tuple)) 

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# Sets

# Sets are unordered and don't allow duplicates
# Since sets are unordered you cannot access elements using index positions
my_set = {'Atul', 'Suvam', 'Prajyoti', 'Shreyas', 'Lekha', 'Anuja', 'Pulkit'}
print(my_set)
# **********************************************************************************************************
# **********On printing you see that they are not in the order in which you mentioned ****************
# **********************************************************************************************************

# Sets are optimised for searching elements within them

print(dir(set))


my_set_2 = {'Aanchal', 'Shreyas', 'Anuja', 'Lekha', 'Vaishnavi', 'Shrikant', 'Shubham', 'Harshad'}


print(my_set)
print(my_set_2)
# To print the common elements in both set we use intersection
print(my_set.intersection(my_set_2))
# To print the ones in my_set not in my_set_2
print(my_set.difference(my_set_2))
# To print the ones in my_set_2 and not in my_set
print(my_set_2.difference(my_set))
# To combine both the sets
print(my_set.union(my_set_2))
