message_1 = 'Hello World. How are you all'

#  upper() and lower() return a copy of the string that you can assign to a variable
print(message_1.upper())
print(message_1.lower())


#  Count occurence in the string
# You can either pass a char or a sub-string as well
# If the char or sub-string does not exist you will get 0
count_o = message_1.count('a')
print(count_o)
print('############################################################################################')
# Difference between find and index is that if the char or sub-string is not present in the string
# For find() you will get -1
# But index() will give you Valueerror 
print(message_1.find('W'))
print(message_1.index('W'))

print('############################################################################################')
message_1.replace('World', 'Universe')
print(message_1)
# There won't be any difference because replace() does not replace the string inplace. It returns a copy which we will have to assign to a variable

replaced_message_1 = message_1.replace('World', 'Universe')
print(message_1)
print(replaced_message_1)


# The dir function shows us all of the attributes and methods that we have access to for that particular type of variable

#  For Example
print(dir(message_1))

# To get to know more about the different methods and attributes for str type use help()
print('###########################################################################################')
print(help(str))

# If you want help with a particuar method 

print('############################################################################################')
print(help(str.upper))
print('############################################################################################')
print(help(str.index))
print('############################################################################################')

print(len(message_1))
# Will padd zeros to the left based on the width of the string
# So in the below example the string message_1 is of length 28
# Therfore if we set the width for zfill as 30 then two zeros will be prepended to the left/start of the string so as to make
# the total width as 30
print(message_1.zfill(30))