from collections import defaultdict


if __name__ == '__main__':
    n, m = map(int, input().split())
    d = defaultdict(list)
    arr_B = []
    for i in range(n):
        d[input()].append(i+1)
    for i in range(m):
        arr_B.append(input())


    for j in arr_B:
        if j in d:
            print(' '.join(map(str, d[j])))
        else:
            print(-1)
