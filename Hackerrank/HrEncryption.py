import math


class Solution():

    def __init__(self):
        pass


    def encrypt(self, s):
        l = len(s)
        sq = math.sqrt(l)
        lower_limit = math.floor(sq)
        upper_limit = math.ceil(sq)
        if lower_limit * upper_limit < l:
            pass
        my_arr = []
        for i in range(0,l,upper_limit):
            my_arr.append(s[i:i+upper_limit])

        # print(my_arr)

        res_arr = []

        for i in range(0, upper_limit):
            res_str = ''
            for char in my_arr:
                # print(i)
                if i >= len(char):
                    break
                res_str += char[i]
            res_arr.append(res_str)

        return res_arr



if __name__ == '__main__':
    my_str = input().strip().replace(" ",'')
    #my_str = "if man was meant to stay on the ground god would have given us roots".replace(" ","")
    # print(my_str)
    S1 = Solution()
    result = S1.encrypt(my_str)
    print(' '.join(result))
    
    
    
    
