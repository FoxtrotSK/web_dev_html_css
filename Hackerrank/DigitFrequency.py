# Given a string, , consisting of alphabets and digits, find the frequency of each digit in the given string.
"""
Sample Input : a11472o5t6
Sample Output: 0 2 1 0 1 1 1 1 0 0 
Explanation:
1 occurs two times 
2 4 5 6 7 occur one time each
0 3 8 9 dont occur at all 
"""

class Solution():


	def __init__(self, my_str):
		self.my_str = my_str

	def find_frequency(self):
		s = self.my_str
		my_dict = dict.fromkeys(list(range(0, 10)),0)
		for i in s:
			if i.isdigit():
				my_dict[int(i)] += 1

		return my_dict




if __name__ == '__main__':
	ip_str = input()
	S1 = Solution(ip_str)
	res = S1.find_frequency()
	for val in res.values():
		print(val, end=" ")