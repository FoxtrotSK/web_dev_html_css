def my_file_handler(my_file):
	with open(my_file, 'r') as f:
		data = f.readlines()
	return data

def remove_newline_char(my_list):
	return [element.replace('\n', '') for element in my_list]

def file_to_write(ip_file, to_write):
	with open(ip_file, 'a') as f:
		f.writelines(to_write)

if __name__ == '__main__':
	file_to_read = 'First_text.txt'
	file_to_read_2 = 'Second_text.txt'
	my_data = my_file_handler(file_to_read)
	print(my_data)
	my_data_from_file_2 = my_file_handler(file_to_read_2)
	# my_data_from_file_2 = remove_newline_char(my_data_from_file_2)
	print(my_data_from_file_2)
	start_index = my_data_from_file_2.index('New_Data\n')
	line_to_write = ['\n']
	for line in my_data_from_file_2[start_index+1: ]:
		line_to_write.append(line)
	file_to_write(file_to_read, line_to_write)

