import math

class Solution():
    
    def __init__(self):
        pass
    
        
    def encrypt(self, s):
        res_arr = []
        l = len(s)
        sq = math.sqrt(l)
        c = math.ceil(sq)
        f = math.floor(sq)
        
        for i in range(c):
            res_arr.append(s[i::c])
        
        return res_arr
    
        
        
if __name__ == '__main__':
    my_str = input().strip().replace(" ", "")
    S1 = Solution()
    result = S1.encrypt(my_str)
    print(" ".join(result))
