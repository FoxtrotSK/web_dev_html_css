def no_idea(arr, A, B):
    my_dict = dict.fromkeys(arr, 0)
    for i in arr:
        if i in my_dict.keys():
            my_dict[i] += 1
    
    intersection_a = A.intersection(arr)
    intersection_b = B.intersection(arr)
    happy = 0
    sad = 0
    for i in intersection_a:
        happy += my_dict[i]
    for j in intersection_b:
        sad += my_dict[j]
        
    return happy - sad

if __name__ == '__main__':
    n,m = map(int, input().split())
    ip_arr = [int(x) for x in input().split()]
    A = set([int(y) for y in input().split()])
    B = set([int(z) for z in input().split()])
    results = no_idea(ip_arr, A, B)
    print(results)
