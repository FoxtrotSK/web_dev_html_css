class Solution():
    
    def __init__(self):
        pass
    
    
    def find_triplets(self, arr, l, d):
        count = 0
        for ele in arr:
            if ele + d in arr and ele+(2*d) in arr:
                count += 1
        return count
    
        
        
        
if __name__ == '__main__':
    n, d = map(int, input().split())
    my_arr = [int(x) for x in input().split()]
    S1 = Solution()
    result = S1.find_triplets(my_arr, n, d)
    print(result)
