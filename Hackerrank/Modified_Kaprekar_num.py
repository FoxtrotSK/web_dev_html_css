class Solution:
    
    def __init(self):
        pass
    
        
    def find_kaprekar(self, l, u):
        ans = []
        if l == 1:
            # Since 1 is a kaprekar number
            ans.append(1)
        for i in range(l,u+1):
            # To avoid error in line 22(because square of 1 2 3 is single digit)
            # There wont be any digit for l and I will get error if I try int(l)
            if i in [1, 2, 3]:
                continue
            d = len(str(i))
            sq = i**2
            sq_str = str(sq)
            r = sq_str[-d:]
            l = sq_str[:-d]
            if (int(r) + int(l)) == i:
                ans.append(i)
        if len(ans) != 0:
            return ans
        else:
            return "INVALID RANGE"
        
        
        
if __name__ == '__main__':
    lower_limit = int(input())
    upper_limit = int(input())
    
    S1 = Solution()
    result = S1.find_kaprekar(lower_limit, upper_limit)
    if type(result) == str:
        print(result)
    else:
        print(" ".join(map(str, result)))