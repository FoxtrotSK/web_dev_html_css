if __name__ == '__main__':
    n = int(input())
    length_of_arr = n
    word_arr = []
    while n:
        n -= 1
        word_arr.append(input())
    my_dict = dict.fromkeys(word_arr, 0)
    for i in range(length_of_arr):
        my_dict[word_arr[i]] += 1
    print(len(my_dict))
    print(' '.join(map(str, my_dict.values())))
