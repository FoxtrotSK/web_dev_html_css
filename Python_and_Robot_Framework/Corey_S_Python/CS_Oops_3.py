# Regular methods in a class automatically take the instance(self) as the first argument 
# class methods automatically take in class(cls) as the first argument
#  static methods don't pass anything automatically.

import datetime

class Employee():
	# Class variable
	raise_amt = 1.04
	num_of_emps = 0
	
	def __init__(self, fname, lname, pay):
		self.fname = fname
		self.lname = lname
		self.pay = pay
		self.email = fname + '.' + lname + '@gmail.com'
		# Increment number of employess for each instance
		Employee.num_of_emps += 1


	def full_name(self):
		return f"My name is {self.fname}-{self.lname}"


	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amt)

	# Unlike regular method that takes self, the class methods automatically take in class(cls) as the first argument
	@classmethod	
	def set_raise_amt(cls, amount):
		cls.raise_amt = amount


	@staticmethod
	def is_workday(day):
		if day.weekday() == 5 or day.weekday() == 6:
			return False

		return True





print('There are {} employees'.format(Employee.num_of_emps))
E1 = Employee('Lewis', 'Hamilton', 600000)
E2 = Employee('Max', 'Verstapen', 500000)
print('There are {} employees'.format(Employee.num_of_emps))
print(E1.pay)
print(E2.pay)
print(Employee.raise_amt)
E1.apply_raise()
E2.apply_raise()
print(E1.pay)
print(E2.pay)

Employee.set_raise_amt(1.06)
E1.apply_raise()
E2.apply_raise()
print(Employee.raise_amt)
print(E1.pay)
print(E2.pay)

my_date = datetime.date(2021, 12, 27)
print(Employee.is_workday(my_date))