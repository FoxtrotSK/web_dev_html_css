# Classes allow us to logically group our data and functions in a way that makes it easier for use to resue or build upon if need be.
# From a class perspective when data means attributes and functions mean methods

# A class is basically a blueprint for creating instances
# Instance variables are variables that belong to a particular instance. i.e They contain data that is unique to each instance



class Employee():
	
	def __init__(self, fname, lname, pay):
		self.fname = fname
		self.lname = lname
		self.pay = pay
		self.email = fname + '.' + lname + '@gmail.com'


	def full_name(self):
		return f"My name is {self.fname}-{self.lname}"





E1 = Employee('Lewis', 'Hamilton', 2921321)
E2 = Employee('Max', 'Verstapen', 2342441)


print(E1)
print(E2)

print(E1.email)
print(E2.email)

print(E1.full_name())
print(E2.full_name())