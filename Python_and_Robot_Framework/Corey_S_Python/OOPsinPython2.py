class Person():

	def __init__(self, fname, lname, age):
		self.fname = fname
		self.lname = lname
		self. age = age


class Employee(Person):
	all_employee = []


	def __init__(self, fname, lname, age, emp_id, salary):
		super().__init__(fname, lname, age)
		self.emp_id = emp_id
		self.salary = salary
		Employee.all_employee.append(self)


	def search_emp(self, name):
		for emp in Employee.all_employee:
			if name not in emp.fname:
				print("Employee {} does not exist in the database. Please check name or ask admin to add {}".format(self.fname, self.fname))
			else:
				print(f"{self.fname} works for our company!")	



if __name__ == '__main__':
	print(Employee.all_employee)
	E1 = Employee('Lewis', 'Hamilton', 36, 343423, 5038484)
	E2 = Employee('Max', 'Verstapen', 26, 324241,3438484)
	E3 = Employee('Corey', 'Schafer', 30, 343421, 23869484)
	E4 = Employee('Daniel', 'Ricardo', 34, 6543423, 1696484)
	E5 = Employee('Kimi', 'Raikenen', 39, 434223, 158484)
	E6 = Employee('Jane', 'Sweeny', 23, 443423, 50765484)
	E7 = Employee('Jhon', 'Doe', 31, 3434231, 5038214)
	E8 = Employee('Sebastian', 'Vettle', 33, 3343423, 5739484)
	print(Employee.all_employee)


	for details in Employee.all_employee:
		print(f"{details.fname}-{details.lname} who is {details.age} years old earns {details.salary}$ per anum!")


	





