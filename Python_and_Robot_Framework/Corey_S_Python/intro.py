import sys
import my_module

# Import the class named Solution from my_module_2 file in folder Sample_Folder
# In order to achieve this add __init__.py file in Sample_Folder
# (__init__.py tells python to treat the collection of .py files as a package)

from Sample_Folder.my_module_2 import Solution

# If you don't want to add __init__.py file in folder you can append your path in the sys.path. 
# sys.path is basically a list
print('###################################################################################################')
print(sys.path)
print('###################################################################################################')

subjects = ['History', 'Science', 'Maths', 'Biology', 'Computer Science', 'Chemistry', 'Physics']

# Create an instance of Solution Class

S1 = Solution()
my_list_with_length = S1.check_len(subjects)

print(my_list_with_length)




