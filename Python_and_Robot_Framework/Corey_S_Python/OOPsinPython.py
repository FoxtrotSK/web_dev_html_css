class Person():

	def __init__(self, fname, lname, age):
		self.fname = fname
		self.lname = lname
		self.age = age
		print('###########################################################################')


	def print_name(self):
		print("Hi All! My name is {}-{}. I am {} years old.".format(self.fname, self.lname, self.age))


class Employee(Person):

	start_age = 25
	all_Employee = []

	def __init__(self, fname, lname, age, emp_id, salary):
		super().__init__(fname, lname, age)
		self.emp_id = emp_id
		self.salary = salary
		Employee.all_Employee.append(self)


	def work_data(self):
		exp = self.age - self.start_age
		print(f"{self.fname} has been working with us for the past {exp} years. {self.fname}'s emp_id is {self.emp_id} and CTC is {self.salary}")



if __name__ == '__main__':
	print(Employee.all_Employee)
	E1 = Employee('Corey', 'Schafer', 29, 1623079, 1300000)
	# print(E1)
	E1.print_name()
	E1.work_data()
	E2 = Employee('Jane', 'Doe', 27, 3632366, 232131)
	E2.print_name()
	E2.work_data()
	E3 = Employee('Max', 'Verstapen', 37, 4314341, 21341231)
	E3.print_name()
	E3.work_data()
	print(Employee.all_Employee)
	print('Our current Employees are:')
	for emp in Employee.all_Employee:
		print("{}-{}".format(emp.fname, emp.lname))