class Solution():

	def __init__(self):
		pass



	def find_prime(self, n1, n2):
		prime = [True for i in range(n2+1)]
		prime[0] = False
		prime[1] = False

		p = 2

		while(p**2 <= n2):

			if prime[p]:

				for i in range(p**2, n2+1, p):
					prime[i] = False
			p += 1

		for j in range(n1, n2+1):
			if prime[j]:
				print(j)




if __name__ == '__main__':
	print('Enter the lower limit: ')
	num1 = int(input())
	print('Enter the upper limit: ')
	num2 = int(input())
	S1 = Solution()
	S1.find_prime(num1, num2)