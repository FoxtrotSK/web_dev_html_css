# Instance variables are set for each instance that we create.
# Class variable belongs to the class. i.e. it is shared among all the instances of the class

class Employee():
	# Class variable
	raise_amt = 1.04
	num_of_emps = 0
	
	def __init__(self, fname, lname, pay):
		self.fname = fname
		self.lname = lname
		self.pay = pay
		self.email = fname + '.' + lname + '@gmail.com'
		# Increment number of employess for each instance
		Employee.num_of_emps += 1


	def full_name(self):
		return f"My name is {self.fname}-{self.lname}"


	def apply_raise(self):
		self.pay = int(self.pay * self.raise_amt)





print('There are {} employees'.format(Employee.num_of_emps))
E1 = Employee('Lewis', 'Hamilton', 600000)
E2 = Employee('Max', 'Verstapen', 500000)

print(E1.full_name())
print(E1.pay)
E1.apply_raise()
print(E1.pay)

print(E2.full_name())
print(E2.pay)
E2.apply_raise()
print(E2.pay)

print('There are {} employees'.format(Employee.num_of_emps))

E3 = Employee('Sebastian', 'Vettel', 463000)
print(E3.full_name())
print('There are {} employees'.format(Employee.num_of_emps))