class CustomException(Exception):
	pass


class MinimumBalanceError(CustomException):

	def __init__(self, phrase):
		self.phrase = phrase

	def __str__(self):
		return str(self.phrase)


def check_balance(b, c, a):
	if b < 500:
		print("You do not have sufficient Balance to make any transactions!")
		raise ValueError
	else:
		if c == 2:
			val = b - a
			if val < 500:
				raise MinimumBalanceError("Operation Denied! The amount you are trying to withdraw will result in inssuficent balance.")
			else:
				b = val
				print("Updated Balance is {}".format(b))



if __name__ == '__main__':
	balance = int(input())
	# choice 1-> Deposit 2-> Withdraw
	choice = int(input())
	# amount to deposit/withdraw based on choice
	amount = int(input())

	try:
		check_balance(balance, choice, amount)

	except ValueError as e:
		print(e)

	except MinimumBalanceError as e:
		print(e)
