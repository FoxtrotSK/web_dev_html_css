# Naming convention for test file. Start with test_<the .py script you are testing>

import unittest
import calc


# Create a class for your tests which inherits from unittest.TestCase


class TestCalc(unittest.TestCase):


	# The method names should start with test_ . This is required

	def test_add(self):
		self.assertEqual(calc.add(10, 5), 15)
		self.assertEqual(calc.add(-10, 5), -5)
		self.assertEqual(calc.add(-1, -5), -6)

	def test_sub(self):
		self.assertEqual(calc.sub(10, 5), 5)
		self.assertEqual(calc.sub(-10, 5), -15)
		self.assertEqual(calc.sub(-1, -5), 4)

	def test_mul(self):
		self.assertEqual(calc.mul(10, 5), 50)
		self.assertEqual(calc.mul(-10, 5), -50)
		self.assertEqual(calc.mul(-1, -5), 5)

	def test_div(self):
		self.assertEqual(calc.div(10, 5), 2)
		self.assertEqual(calc.div(-10, 5), -2)
		self.assertEqual(calc.div(-15, -5), 3)
		self.assertEqual(calc.div(15, 0), 3)

	





if __name__ == '__main__':
	unittest.main()


