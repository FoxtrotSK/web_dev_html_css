import datetime
# from datetime import datetime


tup = (2021, 11, 3)
my_list = []
date = datetime.date(2021, 11, 3)
print(date)
my_list.append(date)
print(my_list)
date_format = datetime.datetime.strftime(date, "%d/%m/%Y")
my_list.append(date_format)
print(my_list)
ts = 1258094605
date_2 = datetime.datetime.fromtimestamp(ts)
print(date_2)
my_list.append(date_2.date())
print(my_list)
tup_2 = (22, 34, 56)
time_1 = datetime.time(tup_2[0], tup_2[1], tup_2[2])
print(time_1)
my_list.append(time_1)
print(my_list)
tup_3 = (2021, 12, 31)
date3 = datetime.date(tup_3[0], tup_3[1], tup_3[2])
print(date3)
number_of_days = datetime.datetime.strftime(date3, "%j")
print(number_of_days)
date4 = datetime.datetime(2021, 11, 3)
# Month name
print(date4.strftime("%B"))

# Day name
print(date4.strftime("%A"))

