#!/bin/python3

import math
import os
import random
import re
import sys


import itertools
#
# Complete the 'usingiter' function below.
#
# The function is expected to return a TUPLE.
# The function accepts following parameters:
#  1. TUPLE tupb
#

def add_1(x, y):
    return x + y

def performIterator(tuplevalues):
    # Write your code here
    res_list = []
    temp_list = []
    l = len(tuplevalues[0])
    print(tuplevalues[0])
    c1 = itertools.cycle(tuplevalues[0])
    for _ in range(l):
        temp_list.append(next(c1))
    res_list.append(tuple(temp_list))
    l2 = len(tuplevalues[1])
    rep = tuple(itertools.repeat(tuplevalues[1][0], l2))
    res_list.append(rep)
    acc1 = tuple(itertools.accumulate(tuplevalues[2], add_1))
    res_list.append(acc1)
    chain_val = tuple(itertools.chain.from_iterable(tuplevalues))
    res_list.append(chain_val)
    filtered = tuple(itertools.filterfalse(lambda x: x%2 == 0, chain_val))
    res_list.append(filtered)
    return tuple(res_list)
    
    
    

if __name__ == '__main__':

    length = int(input().strip())

    qw1 = []
    for i in range(4):
        qw2 = []
        for _ in range(length):
            qw2_item = int(input().strip())
            qw2.append(qw2_item)
        qw1.append(tuple(qw2))
    tupb = tuple(qw1)

    q = performIterator(tupb)
    print(q)

