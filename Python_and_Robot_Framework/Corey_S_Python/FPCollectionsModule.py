
import math
import os
import random
import re
import sys



from collections import Counter, OrderedDict, defaultdict, namedtuple
#
# Complete the 'collectionfunc' function below.
#
# The function accepts following parameters:
#  1. STRING text1
#  2. DICTIONARY dictionary1
#  3. LIST key1
#  4. LIST val1
#  5. DICTIONARY deduct
#  6. LIST list1
#

def collectionfunc(text1, dictionary1, key1, val1, deduct, list1):
    # Write your code here
    d1 = dict(Counter(text1.split()))
    print(dict(sorted(d1.items())))
    
    c1 = Counter(dictionary1)
    c1.subtract(deduct)
    print(dict(c1))
    od = OrderedDict()
    for k, v in zip(key1, val1):
        od[k] = v
        
    od1 = {key: value for key, value in od.items() if key != key1[1]}
    od1[key1[1]] = val1[1]
    print(od1)
    dd = defaultdict(list)
    for i in list1:
        if i%2 == 0:
            dd['even'].append(i)
        else:
            dd['odd'].append(i)
    print(dict(dd))

if __name__ == '__main__':
    from collections import Counter

    text1 = input()
    
    n1 = int(input().strip())
    qw1 = []
    qw2 = []
    for _ in range(n1):
        qw1_item = (input().strip())
        qw1.append(qw1_item)
        qw2_item = int(input().strip())
        qw2.append(qw2_item)
    testdict={}
    for i in range(n1):
        testdict[qw1[i]]=qw2[i]
    collection1 = (testdict)
    
    qw1 = []
    n2 = int(input().strip())
    for _ in range(n2):
        qw1_item = (input().strip())
        qw1.append(qw1_item)
    key1 = qw1
    
    qw1 = []
    n3 = int(input().strip())
    for _ in range(n3):
        qw1_item = int(input().strip())
        qw1.append(qw1_item)
    val1 = qw1

    n4 = int(input().strip())
    qw1 = []
    qw2 = []
    for _ in range(n4):
        qw1_item = (input().strip())
        qw1.append(qw1_item)
        qw2_item = int(input().strip())
        qw2.append(qw2_item)
    testdict={}
    for i in range(n4):
        testdict[qw1[i]]=qw2[i]
    deduct = testdict

    qw1 = []
    n5 = int(input().strip())
    for _ in range(n5):
        qw1_item = int(input().strip())
        qw1.append(qw1_item)
    list1 = qw1

    collectionfunc(text1, collection1, key1, val1, deduct, list1)