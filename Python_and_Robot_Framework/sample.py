
def my_func(data):
	my_list = []
	if type(data) == dict:

		for key, value in data.items():
			my_list.append(int(value))

	l = len(my_list)
	temp = 0
	for i in range(l):
		for j in range(i+1, l):
			if my_list[i] > my_list[j]:
				temp = my_list[i]
				my_list[i] = my_list[j]
				my_list[j] = temp
	return my_list




if __name__ == '__main__':
	my_dict = {'a': '7', 'b': '2', 'c': '1', 'd':'9'}
	result = my_func(my_dict)
	print(result)


