class Solution:

	def __init__(self):
		pass


	def find_bitwise(self, A, B, k):
		bit_max = []
		for i in A:
			for j in B:
				value = i&j
				if value < k:
					bit_max.append(value)
		l = len(bit_max)
		if l == 0:
			return 0

		bit_max.sort()

		return bit_max[-1]
		



if __name__ == '__main__':
	T = int(input())
	result = []
	while T:
		T-= 1
		n,k = map(int, input().split())
		A = [int(i) for i in range(1, n)]
		B = [int(j) for j in range(2, n+1)]
		print(A)
		print(B)
		S1 = Solution()
		result.append(S1.find_bitwise(A, B, k))
	print("\n".join(map(str, result)))
