class Solution:

	def __init__(self):
		pass

	def calculate_fine(self, r_date, d_date):
		rday, rmonth, ryear = map(int, r_date.split())
		dday, dmonth, dyear = map(int, d_date.split())

		if ryear < dyear:
			return 0
		elif ryear > dyear:
			return 10000
		elif ryear == dyear and rmonth > dmonth:
			return 500 * (rmonth - dmonth)
		elif ryear == dyear and rmonth == dmonth and rday > dday:
			return 15 * (rday - dday)
		else:
			return 0




if __name__ == '__main__':
	returned_date = input()
	due_date = input()

	S1 = Solution()
	result = S1.calculate_fine(returned_date, due_date)
	print(result)