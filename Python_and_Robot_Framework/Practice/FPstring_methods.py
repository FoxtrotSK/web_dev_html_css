#!/bin/python3

import math
import os
import random
import re
import sys



#
# Complete the 'strmethod' function below.
#
# The function accepts following parameters:
#  1. STRING para
#  2. STRING spch1
#  3. STRING spch2
#  4. LIST li1
#  5. STRING strf
#

def stringmethod(para, special1, special2, list1, strfind):
    # Write your code here
    word1 = ''
    for word in para:
        if word not in special1:
            word1 += word
            
    rword2 = word1[:70][::-1]
    print(rword2)
    
    rword2 = rword2.replace(" ", '')
    rword2 = '{0}'.join(rword2).format(special2)
    print(rword2)
    l = len(list1)
    cnt = 0
    for ele in list1:
        if ele in para:
            cnt += 1
    if cnt == l:
        print(f"Every string in {list1} were present")
    else:
        print(f"Every string in {list1} were not present")
    
    temp_word1 = word1.split()
    print(temp_word1[:20])
    
    freq_count = dict.fromkeys(temp_word1, 0)
    for ele in temp_word1:
        freq_count[ele] += 1
    count_res = []
    for char, count in freq_count.items():
        if count < 3:
            count_res.append(char)
    print(count_res[-20:])
    
    print(word1.rfind(strfind))
            

if __name__ == '__main__':
    para = input()

    spch1 = input()

    spch2 = input()
    
    qw1_count = int(input().strip())

    qw1 = []

    for _ in range(qw1_count):
        qw1_item = input()
        qw1.append(qw1_item)

    strf = input()

    stringmethod(para, spch1, spch2, qw1, strf)
