# Brute Force approach

def find_all_substrings(ip):
	length_ip = len(ip)
	substring_arr = []
	for i in range(length_ip):
		for j in range(i+1, length_ip+1):
			substring_arr.append(ip[i:j])
	return substring_arr

if __name__ == '__main__':
	ip_string = input('Please Enter your string: ')
	result = find_all_substrings(ip_string)
	print(result)
	print(len(result))