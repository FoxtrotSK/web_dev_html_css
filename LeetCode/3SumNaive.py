class Solution:
    def threeSum(self, arr: List[int]) -> List[List[int]]:
        l = len(arr)
        if l == 0:
            return []
        triplet = []
        arr.sort()
        
        for i in range(0,l-2):
            j = i+1
            k = l-1
            triplet_sum = 0
            while(j<k):
                triplet_sum = arr[i] + arr[j] + arr[k]
                if triplet_sum == 0:
                    triplet.append([arr[i], arr[j], arr[k]])
                    j+=1
                elif triplet_sum < 0:
                    j+=1
                elif triplet_sum > 0:
                    k -= 1
        tl = len(triplet)
        final_res = []
        # for i in range(tl):
        #     if triplet[i] not in triplet[i+1:]:
        #         final_res.append(triplet[i])
                
        final_res = list(set(tuple(sub)for sub in triplet))


        return final_res
