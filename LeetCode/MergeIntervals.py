class Solution():


	def __init__(self):
		pass


	def merge_interval(self, array, l):
		if l <= 1:
			return array
		output = []
		array.sort(key=lambda x:x[0])
		current_interval = array[0]
		output.append(current_interval)
		# print(array)
		# print(output)
		for arr in array[1:]:
			current_begin = current_interval[0] 
			current_end = current_interval[1]
			next_begin = arr[0]
			next_end = arr[1]

			if current_end >= next_begin:
				current_interval[1] = max(current_end, next_end)
			else:
				current_interval = arr
				output.append(current_interval)

		return output




if __name__ == '__main__':
	length = int(input())
	ip_arr = []
	for _ in range(length):
		ip_arr.append([int(x) for x in input().split()])
	# print(ip_arr)
	S1 = Solution()
	result = S1.merge_interval(ip_arr, length)
	print(result)