def find_index(nums, t):
	my_dict = {}
	l = len(nums)
	for i in range(l):
		diff = t - nums[i]
		if diff not in my_dict:
			my_dict[nums[i]] = i
		else:
			return [my_dict[diff]+1, i+1]


if __name__ == '__main__':
	numbers = list(map(int, input().split()))
	target = int(input())
	result = find_index(numbers, target)
	print(result)