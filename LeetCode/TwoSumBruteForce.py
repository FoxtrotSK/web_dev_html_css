# Brute Force Approach
def find_index(arr, t):
	l = len(arr)
	for i in range(l):
		for j in range(i+1, l):
			if arr[i] + arr[j] == t:
				return [i, j]


if __name__ == '__main__':
	nums = [int(x) for x in input().split()]
	target = int(input())
	result = find_index(nums, target)
	print(result)