class Solution:
    def maxArea(self, height: List[int]) -> int:
        l = len(height)
        start_index = 0
        end_index = l - 1
        max_area = 0
        while start_index < end_index:
            if height[start_index] < height[end_index]:
                max_area = max(max_area, height[start_index]*(end_index - start_index))
                start_index += 1
            else:
                max_area = max(max_area, height[end_index]*(end_index - start_index))
                end_index -= 1
        return max_area
        