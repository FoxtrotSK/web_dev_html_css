def find_index(arr, t):
	# Using array***************************
	# ans = []
	# l = len(arr)
	# i = 0
	# while i <=l:
	# 	diff = target - arr[i]
	# 	if diff not in ans:
	# 		ans.append(arr[i])
	# 	else:
	# 		return [ans.index(diff), i]
	# 	i += 1
	# **************************************

	#  Better Approach using dictionary
	my_dict = {}
	l = len(arr)
	for i in range(l):
		diff = t - arr[i]
		if diff not in my_dict:
			my_dict[arr[i]] = i
		else:
			return [my_dict[diff], i]



if __name__ == "__main__":
	nums = [int(x) for x in input().split()]
	target = int(input())
	result = find_index(nums, target)
	print(result)