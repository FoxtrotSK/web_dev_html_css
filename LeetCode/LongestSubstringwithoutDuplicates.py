def longest_substring_wout_repeating(s):
	length_of_s = len(s)
	if length_of_s == 0:
		return 0
	my_dict = dict.fromkeys(s, 0)
	my_dict[s[0]] += 1
	i = 0
	j = 0
	ans = 1
	while j != length_of_s - 1:
		if my_dict[s[j+1]] == 0:
			j += 1
			my_dict[s[j]] = 1
			ans = max(ans, j-i+1)
			print(s[i:j+1], j-i+1)
		else:
			my_dict[s[i]] -= 1
			i += 1
	return ans


if __name__ == '__main__':
	ip_string = input('Enter input string: ')
	result = longest_substring_wout_repeating(ip_string)
	print(result)