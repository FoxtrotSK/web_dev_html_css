# def find_duplicates(arr):
# 	my_set = set(arr)
# 	return sorted(list(my_set))


# if __name__ == '__main__':
# 	nums = [int(x) for x in input().split()]
# 	result = find_duplicates(nums)
# 	print(f"{len(result)} \n{result}")



class Solution:
	def __init__(self):
		pass

	def remove_duplicates(self, arr):
		my_set = set(arr)
		# Sorting since set is an unordered datastructure.
		return sorted(list(my_set))



if __name__ == '__main__':
	nums = [int(x) for x in input().split()]
	S1 = Solution()
	result = S1.remove_duplicates(nums)
	print(f"{len(result)}\n{result}")
