class Solution:
    def intervalIntersection(self, firstList: List[List[int]], secondList: List[List[int]]) -> List[List[int]]:
        f_pointer = 0
        l_pointer = 0
        l1 = len(firstList)
        l2 = len(secondList)
        result_array = []
        val_1 = 0
        val_2 = 0
        while f_pointer < l1 and l_pointer < l2:
            if firstList[f_pointer][0] <= secondList[l_pointer][1] and firstList[f_pointer][1] >= secondList[l_pointer][0]:
                val_1 = max(firstList[f_pointer][0], secondList[l_pointer][0])
                val_2 = min(firstList[f_pointer][1], secondList[l_pointer][1])
                result_array.append([val_1, val_2])
            # print(result_array)

            if firstList[f_pointer][1] > secondList[l_pointer][1]:
                l_pointer += 1
            else:
                f_pointer += 1
        return result_array
        
        