def find_median(arr1, arr2):
	arr1.extend(arr2)
	arr1.sort()
	res = 0
	l = len(arr1)
	res_index = l//2
	if l%2 == 0: 
		return (arr1[res_index] + arr1[(res_index - 1)])/2
	else:
		return arr1[res_index]




if __name__ == '__main__':
	nums1 = [int(x) for x in input().split()]
	nums2 = [int(y) for y in input().split()]
	results = find_median(nums1, nums2)
	print(results)